/*
 * Algoritmo narrado:
 * - Considerando el enunciado, asignar cada variable obtenida:
 *       - 3 personas,
 *       - 5 huevos de juan,
 *       - 3 huevos de manuel,
 *       - 0 huevos de antonio
 *       - 8 en total
 *       - $80 que antonio entregará
 *       - 8/3: una porción es huevos totales dividido en personas totales
 * - Calcular la cantidad de huevos que juan entregó a antonio: juan - una porción
 * - Calcular los huevos que manuel entregó a antonio: manuel - una porción
 * - Usando los valores anteriores, multiplicarlos por el dinero partido por una porción.
 *   Esto entrega el dinero que debe a cada uno.
 * - Imprimir los valores, y explicar por qué no es $50 y $30 a juan y manuel, respectivamente.
 * - Finalizar el programa:
 */

#include <stdio.h>

void calcular(float* deudaJuan, float* deudaManuel) {
	// "3 amigos deciden hacer huevos revueltos."
	int personas = 3,

	// "Juan lleva 5 huevos, Manuel 3 y Antonio ninguno."
		juan = 5,
		manuel = 3,
		antonio = 0,
		huevos = juan + manuel + antonio,

	// "a cambio, Antonio les debe pagar 80 pesos."
		plata = 80;

	// "Hacen un total de 8 huevos revueltos y se reparten en partes iguales"
	// float porcion = huevos / personas;
	float porcion = ((float)huevos) / ((float)personas);

	// "Antonio les debe pagar 80 pesos."
	// juan = 5 huevos; antonio comió `huevos/personas`;
	// entonces juan - antonio = 5 - 8/3
	float huevosRegaladosPorJuan = juan - porcion;

	// manuel = 3 huevos; antonio comió `huevos/personas`;
	// entonces manuel - antonio = 3 - 8/3
	float huevosRegaladosPorManuel = manuel - porcion;

	// ¿Cuánto de los 80 pesos tendrá que dar Antonio a Juan y cuánto a Manuel [...]?

	// precioHuevosDeJuan + precioHuevosDeManuel debería ser $80
	float precioHuevosDeJuan = huevosRegaladosPorJuan * plata / porcion;
	float precioHuevosDeManuel = huevosRegaladosPorManuel * plata / porcion;

	*deudaJuan = precioHuevosDeJuan;
	*deudaManuel = precioHuevosDeManuel;
}

int imprimir(float precioHuevosDeJuan, float precioHuevosDeManuel) {
	printf("antonio le debe $%.0f pesos a juan\n", precioHuevosDeJuan);
	printf("antonio le debe $%.0f pesos a manuel\n", precioHuevosDeManuel);

	printf("es incorrecta la respuesta de que a juan y manuel se le deben $50 y $30\n");
	printf("respectivamente, ya que al calcular la porción de antonio se obtiene que\n");
	printf("no se comió el 100%% de los huevos, así que no debería pagarlos todos\n");

	return 0;
}

int main() {
	float deudaJuan, deudaManuel;
	calcular(&deudaJuan, &deudaManuel);

	imprimir(deudaJuan, deudaManuel);

	return 0;
}
