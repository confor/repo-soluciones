/*
 * Dar vuelta un número
 *
 * Algoritmo narrado:
 * - Crear una variable entero `numero`.
 * - Crear una variable tipo char[], efectivamente un string. Esto va
 *   a contener el comando que se pasará a `system()`.
 * - Solicitar un entero al usuario, asignarlo a `numero`.
 * - Construir el comando "echo %d | rev", usando el número dado por el usuario.
 * - Ejecutar el comando para obtener el "número" invertido.
 * - Finalizar el programa.
 *
 * Problemas:
 * Es capaz de dar vuelta cualquier cosa, no sólo números. A pesar de que
 * se solicita un entero y el resultado cumple con el enunciado,
 * resulta que `rev` puede hacer mucho más. Pero cumple con el enunciado.
 */

#include <stdio.h>
#include <stdlib.h>

int funcion() {
	int numero;
	char comando[256];

	printf("Ingrese un numero: ");
	scanf("%d", &numero);

	snprintf(comando, sizeof(comando), "echo %d | rev", numero);
	system(comando);

	return 0;
}

int main() {
	return funcion();
}
