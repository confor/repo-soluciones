/*
 * Asociación de vinicultores: precio de kilo de uva
 *
 * Algoritmo narrado:
 * - Solicitar precio del kilo de uva
 * - Solicitar tipo de uva (A ó B)
 * - Solicitar tamaño de uva (1 ó 2)
 * - Si el tipo es A,
 *     - Si el tamaño es 1, sumarle $20
 *       si no, sumarle $30
 * - Si el tipo es B,
 *     - Si el tamañño es 1, restarle $30
 *       si no, restarle $50
 * - Presentar precio del kilo
 *
 */

#include <stdio.h>

int main() {
	int precio,
		tamano;

	char tipo;

	printf("Cual es el precio del kilo de uva? ");
	scanf("%d", &precio);

	printf("De que tipo es la uva? (A o B) ");
	scanf("%s", &tipo);

	printf("De que tamano es la uva? (1 o 2) ");
	scanf("%d", &tamano);

	if (tipo == 'A') {
		if (tamano == 1) {
			precio += 20;
		} else {
			precio += 30;
		}
	} else {
		if (tamano == 1) {
			precio -= 30;
		} else {
			precio -= 50;
		}
	}

	printf("El precio del kilo es: $%d.-\n", precio);

	return 0;
}
