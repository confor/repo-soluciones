/*
 *  ( b - a )   primero, calcular la distancia entre los dos puntos
 *    / 2       luego, dividir por dos para obtener el punto medio
 *    + a       finalmente sumar la distancia menor, para incluir
 *              la distancia inicial.
 *              vivan los algoritmos!
 *

                   ---->                   <----
                    A                        B
	curicó -------------------------------------------- talca
                   190                      250
                      200                240
                         210          230
                            220 -- 220
 *
 *
 */

#include <stdio.h>

int main() {
	int a = 190,
		b = 250;

	printf("Se encontraran en %d\n", (b - a) / 2 + a);

	return 0;
}