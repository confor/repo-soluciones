/*
 * Sistema de votos para presidente del centro de alumnos
 *
 * Algoritmo narrado:
 * - Comenzar un bucle:
 *     - Solicitar voto: 1, 2, 3
 *     - Incrementar voto en uno dependiendo del candidato
 *     - Preguntar al usuario si desea salir del bucle. Si sale:
 *         - Terminar bucle.
 *       Si no,
 *         - Volver a primer paso.
 * - Obtener suma de todos los votos
 * - Comparar quien tiene la mayor cantidad de votos
 * - Mostrar candidato ganador
 * - Mostrar porcentajes
 *
 */

#include <stdio.h>

int main() {
	int votosCandUno = 0,
		votosCandDos = 0,
		votosCandTres = 0,
		votosNulos = 0,
		votosTotales;

	float porcentajeUno,
		  porcentajeDos,
		  porcentajeTres,
		  porcentajeNulos;

	printf("Elecciones para presidente del Centro de Alumnos ICB\n");

	int votoActual;
	char comando = ' ';

	while (comando != 'f') {
		printf("Ingrese voto (1, 2 o 3):\n");
		printf("> ");
		scanf("%d", &votoActual);

		switch (votoActual) {
			case 1:
				votosCandUno++;
				break;

			case 2:
				votosCandDos++;
				break;

			case 3:
				votosCandTres++;
				break;

			default:
				votosNulos++;
				break;
		}

		// gracias ayudante roberto :D
		// tuve que básicamente copiar esta linea :(
		// y tampoco lo entiendo
		while( getchar() != '\n' );

		printf("Escriba \"f\" para detener: ");
		scanf("%c", &comando);

		// reiniciar variable
		votoActual = ' ';

	}

	votosTotales = votosNulos + votosCandTres + votosCandDos + votosCandUno;

	printf("votos totales: %d", votosTotales);
	porcentajeUno = votosCandUno / votosTotales * 100;
	porcentajeDos = votosCandDos / votosTotales * 100;
	porcentajeTres = votosCandTres / votosTotales * 100;
	porcentajeNulos = votosNulos / votosTotales;

	printf("\n\n");

	if ( votosCandUno > votosCandDos && votosCandUno > votosCandTres ) {
		printf("El candidato uno gano!!!!!!!!!\n");
	} else if ( votosCandDos > votosCandUno && votosCandDos > votosCandTres ) {
		printf("El candidato dos ha ganado!!!!!!!!!\n");
	} else if ( votosCandTres > votosCandUno && votosCandTres > votosCandDos ) {
		printf("El candidato tres ha ganado!!!!!!!!!\n");
	} else if ( votosCandUno == votosCandDos && votosCandDos == votosCandTres ) {
		printf("Hubo un empate entre todos los candidatos!\n");
	} else if ( votosCandUno == votosCandDos ) {
		printf("Hubo un empate entre el candidato uno y el dos\n");
	} else if ( votosCandDos == votosCandTres ) {
		printf("Hubo un empate entre el candidato dos y el tres\n");
	} else if ( votosCandTres == votosCandUno ) {
		printf("Hubo un empate entre el candidato uno y el tres\n");
	} else {
		printf("error de logica: no se sabe quien gano\n");
	}

	printf("Candidato uno:  %f%%\n", porcentajeUno);
	printf("Candidato dos:  %f%%\n", porcentajeDos);
	printf("Candidato tres: %f%%\n", porcentajeTres);
	printf("Votos nulo: %f%%\n", porcentajeNulos);
}
