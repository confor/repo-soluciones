/*
 * Ejercicio 03
 *
 * Algoritmo narrado:
 * - Solicitar precio inicial del traje
 * - Si el precio es mayor a $2500,
 *     - Calcular descuento: 15% del precio
 * - Si no,
 *     - Calcular descuento: 8% del precio
 * - Restar descuento al precio
 * - Presentar descuento obtenido
 * - Presentar precio final
 *
 */


#include <stdio.h>

int main() {
	printf("Trajes \"El harapiento distinguido\"\n");

	float precio;
	float descuento;

	printf("Ingrese precio inicial: ");
	scanf("%f", &precio);

	if ( precio > 2500 ) {
		// precio - 15%
		descuento = precio * 0.15;
	} else {
		// precio - 8%
		descuento = precio * 0.08;
	}

	precio -= descuento;

	printf("Descuento:    $%.0f\n", descuento);
	printf("Precio final: $%.0f\n", precio);

	return 0;
}