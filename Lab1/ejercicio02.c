/*
 * Ejercicio 02
 *
 * Algoritmo narrado:
 * - Solicitar cantidad de amigos de Pepito
 * - Solicitar valor inicial de la cuenta
 * - Calcular IVA: 19% de la cuenta
 * - Calcular propina: 10% de la cuenta
 * - Calcular total: `cuenta + iva + propina`, dividido en
     Pepito más sus amigos ( `1 + amigos` )
 * - Presentar IVA, propina, total
 * - Presentar valor que cada uno deberá pagar
 *
 */


#include <stdio.h>

int main() {
	float cuenta,
		  iva,
		  propina,
		  pago;

	int amigos;

	printf("Con cuantos amigos salio a comer Pepe? ");
	scanf("%d", &amigos);

	printf("Cuanto salio la cuenta? ");
	scanf("%f", &cuenta);

	// cuenta + 19% iva + 10% propina
	iva = cuenta / 100 * 19;
	propina = cuenta / 100 * 10;
	cuenta = cuenta + iva + propina;

	// cuenta dividida en pepe + amigos
	pago = cuenta / ( 1 + amigos );

	printf("IVA:     $%.2f\n", iva);
	printf("Propina: $%.2f\n", propina);
	printf("Total:   $%.2f\n\n", cuenta);

	printf("Cada uno debe pagar $%.0f.-", pago);

	return 0;
}
