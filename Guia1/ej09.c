/*
 * Calcular dinero total recibido dado un sueldo más un
 * porcentaje de comisión por ventas realizadas
 *
 * Enunciado:
 * "Un vendedor recibe un sueldo base mensual más un porcentaje
 * extra como comisión de sus ventas. El porcentaje esta semana
 * corresponde al 7% de las ventas realizadas por el vendedor.
 * Indique cuánto dinero recibe en total al final del mes y
 * cuanto corresponde a la comisión."
 *
 * Narrado:
 * - Solicitar sueldo: Leer variable y asignar a `sueldo`
 * - Solicitar ventas: Leer variable y asignar a `ventas`
 * - Calcular comisión extra: 7% de ventas
 * - Sumar `sueldo + ventas`, obtener sueldo total
 * - Presentar sueldo total al usuario
 */

#include <stdio.h>

int main() {
	int sueldo, ventas;
	float porcentaje_extra = 7;
	float sueldo_extra;

	printf("Cual es su sueldo? $");
	scanf("%i", &sueldo);

	printf("Cuales fueron sus ventas? $");
	scanf("%i", &ventas);

	sueldo_extra = (ventas / 100) * porcentaje_extra;

	printf("Sueldo base: $%i\n", sueldo);
	printf("Comision: $%.1f (%.0f%%)\n", sueldo_extra, porcentaje_extra);
	printf("Sueldo total: $%.1f\n", sueldo + sueldo_extra);

	return 0;
}
