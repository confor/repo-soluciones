/*
 * Transformar libras y pies a kilogramos y metros respectivamente.
 *
 * "Dado como datos el nombre de un dinosaurio, su peso y su longitud,
 * expresados estos dos últimos en libras y pies respectivamente,
 * escriba el nombre del dinosaurio, su peso expresado en kilogramos
 * y su longitud expresada en metros. Considere:
 *   1 libra = 0.45 kilogramos
 *   1 pie = 0.3048 metros
 *
 * Narrado:
 * - Obtener el nombre del dinosaurio, asignar a `nombre`
 * - Obtener el peso del dinosaurio en libras, asignar a `peso`
 * - Obtener la longitud del dinosaurio en pies, asignar a `longitud`
 * - Multiplicar peso en libras por 0.45 para obtener valor en kilogramos
 * - Multiplicar peso en pies por 0.3048 para obtener valor en metros
 * - Presentar peso en kilogramos
 * - Presentar peso en metros
 */

#include <stdio.h>

const float LIBRA_EN_KG = 0.45; // kg
const float PIE_EN_M = 0.3048; // metros

int main() {
	// arreglo de caracteres = cadena de texto
	char nombre[128];

	float peso, longitud;

	printf("Nombre del dinosaurio: ");
	scanf("%s", nombre);

	printf("\nPeso del dinosaurio, en libras: ");
	scanf("%f", &peso);

	printf("\nLongitud del dinosaurio, en pies: ");
	scanf("%f", &longitud);

	// convertir de libras a kilogramos
	peso = peso * LIBRA_EN_KG;
	// convertir de pies a metros
	longitud = longitud * PIE_EN_M;

	printf("Peso de %s: %.2f kg\n", nombre, peso);
	printf("Longitud de %s: %.2f m\n", nombre, longitud);

	return 0;
}
