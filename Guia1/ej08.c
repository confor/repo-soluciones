/*
 * Calcular el polinomio:
 *   p(x): x^3 + 2x^2 + 3x + 5
 *
 * Narrado:
 * - Solicitar valor numérico real: Leer número y asignarlo a `x`
 * - Calcular `x^3 + 2x^2 + 3x + 5` y asignarlo a `y`
 * - Presentar valor de `y`
 *
 */

#include <stdio.h>
// #include <math.h>

int main() {
	float x, y;

	printf("Evaluar el siguiente polinomio:\n");
	printf("f(x): x^3 + 2x^2 + 3x + 5\n");

	printf("Ingresar un valor real para x: ");
	scanf("%f", &x);

	// y = pow(x, 3) + 2 * pow(x, 2) + 3 * x + 5;
	y = (x * x * x) + ( 2 * (x * x) ) + ( 3 * x ) + 5;
	printf("f(x): %.1f\n", y);

	return 0;
}
