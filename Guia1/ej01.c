/*
 * Calcular promedio
 *
 * Enunciado:
 * "Dada la matrícula y 3 calificaciones de un alumno obtenida a lo largo
 * del semestre, imprima la matrícula del alumno y el promedio de sus
 * calificaciones. Las calificaciones deben ser entregas por el usuario"
 *
 * Algoritmo narrado:
 * - Solicitar número de matrícula
 * - Leer input y asignarlo a una variable `matricula`
 * - Solicitar nota #1: Leer input y asignar a float `notaA`
 * - Solicitar nota #2: Leer input y asignar a float `notaB`
 * - Solicitar nota #3: Leer input y asignar a float `notaC`
 * - Sumar todas las notas y dividir por tres para obtener el promedio
 * - Presentar el promedio del alumno
 */

#include <stdio.h>

int main() {
	int matricula;
	float notaA, notaB, notaC, promedio;

	printf("Ingrese numero de matricula del alumno: ");
	scanf("%i", &matricula);

	printf("\nIngrese nota #1: ");
	scanf("%f", &notaA);

	printf("\nIngrese nota #2: ");
	scanf("%f", &notaB);

	printf("\nIngrese nota #3: ");
	scanf("%f", &notaC);

	promedio = ( notaA + notaB + notaC ) / 3;
	printf("\nEl promedio del alumno %d es %.2f", matricula, promedio);

	return 0;
}
