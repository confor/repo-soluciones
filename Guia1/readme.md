## Sol. algorítmicas: Guía 1

### 01. Calcular promedio
- Solicitar número de matrícula
- Leer input y asignarlo a una variable `matricula`
- Solicitar nota #1: Leer input y asignar a float `notaA`
- Solicitar nota #2: Leer input y asignar a float `notaB`
- Solicitar nota #3: Leer input y asignar a float `notaC`
- Sumar todas las notas y dividir por tres para obtener el promedio
- Presentar el promedio del alumno

### 02. Calcular cuadrado y cubo de un número
Nota: El enunciado no pide obtener los valores del usuario.

- Asignar número `n`
- Calcular `n * n`
- Calcular `n * n * n`
- Presentar los valores

### 03. Calcular hipotenusa de un triángulo
- Obtener valor numérico del cateto A, asignarlo a `a`
- Obtener valor numérico del cateto B, asignarlo a `b`
- Calcular `a * a + b * b` y luego obtener su raíz cuadrada, esta es la hipotenusa
- Presentar el valor de hipotenusa

### 04. Calcular perímetro y superficie dada una base y altura.
- Obtener un número real: base del rectángulo
- Obtener un número real: altura del rectángulo
- Calcular `2(base+altura)` para el perímetro
- Calcular `base * altura` para la superficie
- Presentar los valores de perímetro y superficie

### 05. Transformar libras a kg y pies a metros
- Obtener el nombre del dinosaurio, asignar a `nombre`
- Obtener el peso del dinosaurio en libras, asignar a `peso`
- Obtener la longitud del dinosaurio en pies, asignar a `longitud`
- Multiplicar peso en libras por 0.45 para obtener valor en kilogramos
- Multiplicar peso en pies por 0.3048 para obtener valor en metros
- Presentar peso en kilogramos
- Presentar peso en metros

### 06. Calcular precio de galones de bencina
- Obtener cantidad de galones vendidos
- Transformar galones a litros
- Multiplicar litros vendidos por precio de un litro
- Presentar precio de los galones vendidos

### 07. Transformar días a segundos
- Obtener cantidad de días del usuario
- Multiplicar días por segundos en un día
- Presentar segundos

### 08. Calcular el polinomio
```
p(x): x³ + 2x² + 3x + 5
```

- Solicitar valor numérico real: Leer número y asignarlo a `x`
- Calcular `x^3 + 2x^2 + 3x + 5` y asignarlo a `y`
- Presentar valor de `y`

### 09. Calcular sueldo total
Nota: El enunciado da el porcentaje de ventas de una semana, pero explica que el sueldo es del mes, que tiene cuatro semanas. ¿Falta información?
- Solicitar sueldo: Leer variable y asignar a `sueldo`
- Solicitar ventas: Leer variable y asignar a `ventas`
- Calcular comisión extra: 7% de ventas
- Sumar `sueldo + ventas`, obtener sueldo total
- Presentar sueldo total al usuario

### 10. Calcular costo de un viaje
- Solicitar kilómetros recorridos: Leer variable y asignar a `km`
- Solicitar costo por kilómetro: Leer variable y asignar a `costo`
- Multiplicar `costo` por `km` recorrido, asignarlo a una variable `total`
- Presentar costo total del viaje
