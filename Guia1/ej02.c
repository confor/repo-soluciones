/*
 * Calcular cuadrado y cubo de un n�mero
 *
 * Narrado:
 * - Asignar n�mero `n`
 * - Calcular `n * n`
 * - Calcular `n * n * n`
 * - Presentar los valores
 */

#include <stdio.h>

int main() {
	int n = 8;

	int cuadrado = n * n,
		cubo = n * n * n;

	printf("Cuadrado de %d: %d\n", n, cuadrado);
	printf("Cubo de %d: %d", n, cubo);

	return 0;
}
