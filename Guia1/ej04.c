/*
 * Calcular perímetro y superficie dada una base y altura.
 *
 * Narrado:
 * - Obtener un número real: base del rectángulo
 * - Obtener un número real: altura del rectángulo
 * - Calcular `2(base+altura)` para el perímetro
 * - Calcular `base * altura` para la superficie
 * - Presentar los valores de perímetro y superficie
 */

#include <stdio.h>

int main() {
	float base, altura, perimetro, superficie;

	printf("Calcular superficie y perimetro de un rectangulo");

	printf("\nIngresar base: ");
	scanf("%f", &base);

	printf("\nIngresar altura: ");
	scanf("%f", &altura);

	perimetro = (base + altura) * 2;
	superficie = base * altura;

	printf("Perimetro: %.2f\n", perimetro);
	printf("Superficie: %.2f\n", superficie);

	return 0;
}
