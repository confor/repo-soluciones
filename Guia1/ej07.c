/*
 * Transformar d�as a segundos
 *
 * Narrado:
 * - Obtener cantidad de d�as del usuario
 * - Multiplicar d�as por segundos en un d�a
 * - Presentar segundos
 */

#include <stdio.h>

// 1 dia = 24*60*60 = 86400 segundos
const int DIAS_EN_SEG = 24 * 60 * 60;

int main() {
	int dias, segundos;

	printf("Ingrese numero de dias: ");
	scanf("%i", &dias);

	segundos = dias * DIAS_EN_SEG;
	printf("%i dias = %i segundos", dias, segundos);

	return 0;
}
