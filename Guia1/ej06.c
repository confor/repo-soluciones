/*
 * Calcular precio de galones de bencina
 *
 * Narrado:
 * - Obtener cantidad de galones vendidos
 * - Transformar galones a litros
 * - Multiplicar litros vendidos por precio de un litro
 * - Presentar precio de los galones vendidos
 */

#include <stdio.h>

const float GALON_EN_L = 3.785;
const int PRECIO_LITRO = 820;

int main() {
	float vendido, precio;

	printf("Cuantos galones se vendieron? ");
	scanf("%f", &vendido);

	precio = vendido * GALON_EN_L * PRECIO_LITRO;

	printf("El precio de %.1f galones es $%.1f", vendido, precio);
	return 0;
}
