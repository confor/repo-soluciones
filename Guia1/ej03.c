/*
 * Calcular hipotenusa de un tri�ngulo
 *
 * c� = a� + b�
 * c = sqrt( a * a + b * b )
 *
 * Narrado:
 * - Obtener valor num�rico del cateto A, asignarlo a `a`
 * - Obtener valor num�rico del cateto B, asignarlo a `b`
 * - Calcular `a * a + b * b` y luego obtener su ra�z cuadrada, esta es la hipotenusa
 * - Presentar el valor de hipotenusa
 */

#include <stdio.h>
#include <math.h>

int main() {
	int a = 3,
		b = 4;

	// sqrt( a * a + b * b )
	float c = sqrt(pow(a, 2) + pow(b, 2));

	printf("Catetos: %d, %d\n", a, b);
	printf("Hipotenusa: %.2f", c);

	return 0;
}
