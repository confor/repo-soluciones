/*
 * Calcular costo de un viaje
 *
 * Enunciado:
 * "La compa��a de autobuses "Camino al cielo" requiere determinar
 * el costo que tendr� el boleto de un viaje sencillo, esto basado
 * en los kil�metros por recorrer y en el costo por kil�metro.""
 *
 * Narrado:
 * - Solicitar kil�metros recorridos: Leer input y asignar a `km`
 * - Solicitar costo por kil�metro: Leer input y asignar a `costo`
 * - Multiplicar `costo` por `km` recorrido, asignarlo a una variable `total`
 * - Presentar total costo del viaje
 *
 */

#include <stdio.h>

int main() {
	float km, costo, total;

	printf("Buses \"Camino al Cielo\"\n");
	printf("    ______________________\n");
	printf("   |,----.,----.,----.,--.\\\n");
	printf("   ||    ||    ||    ||   \\\\\n");
	printf("   |`----'`----'|----||----\\`.\n");
	printf("   [            |   -||- __|(|\n");
	printf("   [  ,--.      |____||.--.  |\n");
	printf("   =-(( `))-----------(( `))==\n");
	printf("      `--'             `--'\n");

	printf("\nIngresar kilometros recorridos: ");
	scanf("%f", &km);

	printf("\nIngresar costo por km: ");
	scanf("%f", &costo);

	total = costo * km;
	printf("\nCosto del viaje: $%.0f\n", total);

	return 0;
}
