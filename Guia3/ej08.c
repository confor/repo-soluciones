#include <stdio.h>

float funcion(float x) {
	return 3 * x + 2 * (x * x) + 3 * x + 5;
}

int main() {
	float x;

	printf("Ingrese un numero para x: ");
	scanf("%f", &x);

	float valor = funcion(x);
	printf("El valor de f(%f) es %.1f\n", x, valor);

	return 0;
}
