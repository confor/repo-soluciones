#include <stdio.h>

int pow2(int n, int e) {
	int i = 0,
		resultado = 1;

	for(; i < e; i++)
		resultado *= n;

	return resultado;
}

void calculo(int limite) {
	int i,
		suma = 0;

	// el enunciado pide iniciar desde el 1 :(
	for(i = 1; i <= limite; i++) {
		suma += pow2(i, i);
	}

	printf("La suma es %d\n", suma);
}

int main() {
	float n = 0;
	while (n <= 0) {
		printf("Ingrese un limite: ");
		scanf("%f", &n);
	}

	calculo(n);
}
