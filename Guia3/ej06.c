#include <stdio.h>

void logica(int hora, int minuto, int segundo) {
	printf("Ahora:\n%02d:%02d:%02d\n\n", hora, minuto, segundo);
	
	minuto += 1;
	segundo += 1;

	while (segundo >= 60) {
		segundo -= 60;
		minuto += 1;
	}

	while (minuto >= 60) {
		minuto -= 60;
		hora += 1;
	}

	if (hora >= 24) {
		hora -= 24;
	}

	printf("En un minuto y un segundo:\n%02d:%02d:%02d\n", hora, minuto, segundo);
}

int main() {
	int hora = 0;
	while (1) {
		printf("Ingrese una hora [0, 24): ");
		scanf("%d", &hora);

		if(hora >= 0 && hora < 24)
			break;
	}

	int minuto = 0;
	while (1) {
		printf("Ingrese un minuto [0, 60): ");
		scanf("%d", &minuto);

		if(minuto >= 0 && minuto < 60)
			break;
	}

	int segundo = 0;
	while (1) {
		printf("Ingrese un segundo [0, 60): ");
		scanf("%d", &segundo);

		if(segundo >= 0 && segundo < 60)
			break;
	}

	logica(hora, minuto, segundo);

	return 0;
}