#include <stdio.h>

float calculo(float a, float b, float limite) {
	printf("%.0f\n", a);

	if(limite == 0)
		return 0;

	return calculo(b, a + b, limite - 1);
}

int main() {
	float n = 0;
	while (n <= 0) {
		printf("Ingrese un limite: ");
		scanf("%f", &n);
	}

	calculo(0, 1, n - 1);

	return 0;
}
