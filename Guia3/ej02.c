#include <stdio.h>

int calculo(int numero) {
	printf("%d\n", numero);

	if(numero == 1)
		return numero;

	if(numero % 2 == 0)
		return calculo(numero / 2);
	else
		return calculo(numero * 3 + 1);
}

int main() {
	int numero;

	printf("Ingrese un numero: ");
	scanf("%d", &numero);

	calculo(numero);
	return 0;
}
