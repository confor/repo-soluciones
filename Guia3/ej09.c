#include <stdio.h>
#include <string.h>

char *fuerza_bruta(int n) {
	switch(n) {
		case 0:
			return "Cero";

		case 1:
			return "Uno";

		case 2:
			return "Dos";

		case 3:
			return "Tres";

		case 4:
			return "Cuatro";

		case 5:
			return "Cinco";

		case 6:
			return "Seis";

		case 7:
			return "Siete";

		case 8:
			return "Ocho";

		case 9:
			return "Nueve";

		case 10:
			return "Diez";

		case 11:
			return "Once";

		case 12:
			return "Doce";

		case 13:
			return "Trece";

		case 14:
			return "Catorce";

		case 15:
			return "Quince";

		case 16:
			return "Dieciseis";

		case 17:
			return "Diecisiete";

		case 18:
			return "Dieciocho";

		case 19:
			return "Diecinueve";

		case 20:
			return "Veinte";

		case 21:
			return "Veintiuno";

		case 22:
			return "Veintidos";

		case 23:
			return "Veintitres";

		case 24:
			return "Veinticuatro";

		case 25:
			return "Veinticinco";

		case 26:
			return "Veintiseis";

		case 27:
			return "Veintisiete";

		case 28:
			return "Veintiocho";

		case 29:
			return "Veintinueve";

		case 30:
			return "Treinta";

		case 31:
			return "Treinta y uno";

		case 32:
			return "Treinta y dos";

		case 33:
			return "Treinta y tres";

		case 34:
			return "Treinta y cuatro";

		case 35:
			return "Treinta y cinco";

		case 36:
			return "Treinta y seis";

		case 37:
			return "Treinta y siete";

		case 38:
			return "Treinta y ocho";

		case 39:
			return "Treinta y nueve";

		case 40:
			return "Cuarenta";

		case 41:
			return "Cuarenta y uno";

		case 42:
			return "Cuarenta y dos";

		case 43:
			return "Cuarenta y tres";

		case 44:
			return "Cuarenta y cuatro";

		case 45:
			return "Cuarenta y cinco";

		case 46:
			return "Cuarenta y seis";

		case 47:
			return "Cuarenta y siete";

		case 48:
			return "Cuarenta y ocho";

		case 49:
			return "Cuarenta y nueve";

		case 50:
			return "Cincuenta";

		case 51:
			return "Cincuenta y uno";

		case 52:
			return "Cincuenta y dos";

		case 53:
			return "Cincuenta y tres";

		case 54:
			return "Cincuenta y cuatro";

		case 55:
			return "Cincuenta y cinco";

		case 56:
			return "Cincuenta y seis";

		case 57:
			return "Cincuenta y siete";

		case 58:
			return "Cincuenta y ocho";

		case 59:
			return "Cincuenta y nueve";

		case 60:
			return "Sesenta";

		case 61:
			return "Sesenta y uno";

		case 62:
			return "Sesenta y dos";

		case 63:
			return "Sesenta y tres";

		case 64:
			return "Sesenta y cuatro";

		case 65:
			return "Sesenta y cinco";

		case 66:
			return "Sesenta y seis";

		case 67:
			return "Sesenta y siete";

		case 68:
			return "Sesenta y ocho";

		case 69:
			return "Sesenta y nueve";

		case 70:
			return "Setenta";

		case 71:
			return "Setenta y uno";

		case 72:
			return "Setenta y dos";

		case 73:
			return "Setenta y tres";

		case 74:
			return "Setenta y cuatro";

		case 75:
			return "Setenta y cinco";

		case 76:
			return "Setenta y seis";

		case 77:
			return "Setenta y siete";

		case 78:
			return "Setenta y ocho";

		case 79:
			return "Setenta y nueve";

		case 80:
			return "Ochenta";

		case 81:
			return "Ochenta y uno";

		case 82:
			return "Ochenta y dos";

		case 83:
			return "Ochenta y tres";

		case 84:
			return "Ochenta y cuatro";

		case 85:
			return "Ochenta y cinco";

		case 86:
			return "Ochenta y seis";

		case 87:
			return "Ochenta y siete";

		case 88:
			return "Ochenta y ocho";

		case 89:
			return "Ochenta y nueve";

		case 90:
			return "Noventa";

		case 91:
			return "Noventa y uno";

		case 92:
			return "Noventa y dos";

		case 93:
			return "Noventa y tres";

		case 94:
			return "Noventa y cuatro";

		case 95:
			return "Noventa y cinco";

		case 96:
			return "Noventa y seis";

		case 97:
			return "Noventa y siete";

		case 98:
			return "Noventa y ocho";

		case 99:
			return "Noventa y nueve";

		case 100:
			return "Cien";

		case 420:
			return ":D blaze it";
	}

	return "desconocido";
}

int main() {
	int n = 0;

	while(1) {
		printf("Entregue un numero: ");
		scanf("%d", &n);

		// n debe cumplir que:
		// 0 <= n < 100
		if(n >= 0 || n < 100)
			break;
	}

	printf("El numero es %s\n", fuerza_bruta(n));

	return 0;
}
