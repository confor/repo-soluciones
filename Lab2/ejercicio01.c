/*
 * Contar números primos menores a N
 *
 * Algoritmo narrado:
 * - Inicializar un entero `límite` y número actual para for loop, contador
 *   para total de primos, y contador para los divisores.
 * - Pedir un número entero N
 * - Para cada número I entre 0 y N,
 *     - Contar divisores desde 1 hasta I: N debe ser divisible sólo entre 1 y N
 *     - Si los divisores de N son igual a 2, añadir +1 a contador de primos
 * - Mostrar contador de primos obtenidos
 * - Fin del programa
 */

#include <stdio.h>

int main() {
	int limite, total, numeroActual, j, divisores;

	printf("Contar primos menores que: ");
	scanf("%d", &limite);

	// probar todos los numeros
	for (numeroActual = 0; numeroActual < limite; numeroActual++) {
		divisores = 0;

		for (j = 1; j <= numeroActual; j++)
			if (numeroActual % j == 0)
				divisores++;

		if (divisores == 2)
			total++;
	}

	printf("Hay %d primos menores que %d\n", total, limite);

	return 0;
}
