/*
 * Contador de "Los gloriosos"
 *
 * Algoritmo narrado:
 * - Iterar I entre [0, 5[
 *     - Solicitar goles propios para el partido I
 *     - Solicitar goles enemigos para el partido I
 *     - Si los goles propios son iguales a los goles enemigos,
 *         - Agregar +1 a los puntos
 *     - Si no, si los goles propios son mayores que los goles enemigos,
 *         - Agregar +3 a los puntos
 * - Mostrar puntos del equipo propio.
 * - Fin
 *
 */

#include <stdio.h>

int main() {
	printf("Contador del equipo de futbol \"Los gloriosos\"\n");
	
	int puntos = 0,
		i;
	
	int golesPropios,
		golesEnemigos;
	
	for (i = 0; i < 5; i++) {
		printf("Goles propios del partido %d: ", i + 1);
		scanf("%d", &golesPropios);
		printf("Goles enemigos del partido %d: ", i + 1);
		scanf("%d", &golesEnemigos);

		if (golesPropios == golesEnemigos)
			puntos += 1;
		else if( golesPropios > golesEnemigos)
			puntos += 3;
	}

	printf("\nEl equipo propio tiene %d puntos\n", puntos);
	
	return 0;
}
