/*
 * Contar cúantos primos hay hasta N
 *
 * Algoritmo narrado:
 * - Solicitar un límite entre [1, 9]
 * - Iterar I entre [1, límite]:
 *     - Iterar J entre [1, I]:
 *         - Imprimir J más un espacio
 *         - Si J es igual a I,
 *             - Iterar K entre [J-1, 0]
 *                 - Imprimir K más un espacio
 *     - Imprimir un salto de línea
 * - Fin
 */

#include <stdio.h>

int main() {
	int limite;
	int i, j, k;
	
	while (limite < 1 || limite > 9) {
		printf("Ingrese limite: ");
		scanf("%d", &limite);
	}
	
	for (i = 1; i <= limite; i++) {
		for (j = 1; j <= i; j++) {
			printf("%d ", j);
			
			if (j == i) {
				for (k = j - 1; k > 0; k--) {
					printf("%d ", k);
				}
			}
		}
		
		printf("\n");
	}
	
	return 0;
}
