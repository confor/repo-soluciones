#include <stdio.h>

// gracias profe fabio
#define true 1
#define false 0

int main() {
	int mayorAUnAnio = false,
		peso = 0;
	
	// 100 ml = 500 mg droga
	// 0.2 mg por kilo
	// 1 mg = 25 gotas
	const float asd = 25 * 0.2;
	
	printf("Su hijo tiene MAS de un anio?\nIngrese 0 para no, 1 para si: ");
	scanf("%d", &mayorAUnAnio);
	
	// una sanitización de mala calidad:
	// sólo dejar que sea 0 o 1
	if (mayorAUnAnio != false)
		mayorAUnAnio = true;

	//if(mayorAUnAnio == true)	
	//	printf("su hijo tiene mas de un anio");
	//else
	//	printf("su hijo tiene menos de un anio");

	printf("cuanto pesa su hijo? en kilogramos: ");
	scanf("%d", &peso);

	printf("advertencia: la automedicacion puede tener resultados no deseados\n");
	printf("advertencia: considere llevar su hijo al doctor mas cercano\n");
	printf("advertencia: y dejar de guiarse por lo que dicen paginas de internet\n\n");

	float what, how;

	if(mayorAUnAnio == true) {
		what = peso * asd;
		printf("la dosis para un infante MAYOR a un anio que pesa %d kilos es %.1f gotas de medicamento una vez al dia", peso, what);
	
	} else {
		what = peso * asd;
		how = what / 3;
		int aaaaa = 24 / 3;
		printf("la dosis para un infante MENOR a un anio que pesa %d kilos es %.1f gotas, pero debe ser dividida en tres dosis a lo largo de un dia, entonces se deben dar %.1f gotas cada %d horas", peso, what, how, aaaaa);
	}
	
	return 0;
}