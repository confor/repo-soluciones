/*
 * Función `magia`:
 *     - Si mes (`i`) es menor a 20, retornar.
 *     - Calcular valor para este mes: 2^i * 10
 *     - Imprimir valor de este mes.
 *     - Sumar valor de este mes más el siguiente.
 *
 * Función `imprimir`:
 *     - Imprimir el total de los 20 meses.
 *
 * Función `main`:
 *     - Llamar a `imprimir()` con el argumento `magia()`, con el argumento 1.
 *     - Finalizar.
 *
 */

#include <stdio.h>
#include <math.h>

int magia(int i) {
	if(i > 20)
		return 0;

	int este_mes = pow(2, i - 1) * 10;

	printf("Mes %02d: $%d\n", i, este_mes);

	return este_mes + magia(i + 1);
}

void imprimir(int total) {
	printf("Al pasar 20 meses, el total fue $%d\n", total);
}

int main() {
	imprimir(magia(1));
	return 0;
}
