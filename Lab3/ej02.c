/*
 * Imprimir un termometro dada una temperatura
 *
 * Función `get_temp`:
 *     - Solicitar un valor al usuario, entre 0 y 50. asignarlo
 *       a la variable `temp`.
 *     - Retornar el valor de la variable `temp`.
 *
 * Función `print_temp`:
 *     - Imprimir las etiquetas del termómetro.
 *     - Repetir "-" `i` veces hasta la temperatura dada.
 *
 * Función `main`:
 *     - Llamar a `get_temp` y obtener temperatura, asignarla a `temp`.
 *     - Llamar a `print_temp` con el argumento `temp`, obtenido
 *       de la función anterior.
 *     - Fin del programa.
 *
 */

#include <stdio.h>

float get_temp() {
	float temp = 0;

	while(temp <= 0 || temp > 50) {
		printf("Ingrese una temperatura (0 - 50): ");
		scanf("%f", &temp);
	}

	return temp;
}

void draw_thermo(float temp) {
	int i;

	printf("0         10        20        30        40        50\n");
	printf("|         |         |         |         |         |\n");

	for (i = 0; i < temp; i++) {
		printf("-");
	}

	printf("*\n");
}

int main() {
	float temp = get_temp();

	draw_thermo(temp);

	return 0;
}
