/*
 * Calular saldo de una persona
 *
 * Función `saldo_total`:
 *     - Retornar el total de las horas trabajadas, multiplicado
 *       por el saldo por hora.
 *
 * Función `mostrar_saldo`:
 *     - Imprimir el saldo.
 *
 * Función `main`:
 *     - Inicializar las variables necesarias:
 *           - `i`: contador.
 *           - `horas_totales`: horas sumadas, todos los dias.
 *           - `saldo_por_hora`: el dinero de una hora.
 *           - `horas`: temporal, para aceptar una variable por scanf.
 *     - Solicitar el saldo por hora. Asignar a `saldo_por_hora`.
 *     - Inicio de búcle: Iterar i entre [0, 6)
 *           - Solicitar hora para el día `i`.
 *           - Sumar las horas del día a las horas totales.
 *     - Llamar a `mostrar_saldo`, el primer argumento son las horas trabajadas
 *       y el segundo es `saldo_total()`, que calcula el saldo total.
 *
 */

#include <stdio.h>

float saldo_total(float horas, float saldo_por_hora) {
	return horas * saldo_por_hora;
}

void mostrar_saldo(float horas, float saldo) {
	printf("Por trabajar %.0f horas, el saldo es $%.1f\n", horas, saldo);
}

int main() {
	int i;

	float horas_totales,
		  saldo_por_hora,
		  horas;

	printf("Ingrese saldo por hora: ");
	scanf("%f", &saldo_por_hora);

	for(i = 0; i < 6; i++) {
		horas = 0;
		printf("Ingrese horas trabajadas el dia %d: ", i + 1);
		scanf("%f", &horas);

		horas_totales += horas;
	}

	mostrar_saldo(horas_totales, saldo_total(horas_totales, saldo_por_hora));

	return 0;
}
