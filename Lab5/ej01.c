/* Apuestas
 * ========
 *
 * Notas:
 * - Es necesario incluir `stdlib.h` para poder usar `rand()`, y
 *   también `time.h` para poder darle un seed "único" a `rand`.
 * - Para poder ser más ordenado, se han definido booleans en las
 *   líneas 59 y 60.
 * - `int aleatorio(int min, int max)` devuelve un número
     pseudo-aleatorio entre `min` y `max`.
 * 
 * Algoritmo narrado:
 * - Primero, en `main`, se debe dar un seed para `rand`, y
 *   con `time(null)` es suficiente.
 * - Luego, llamar a la función `leer_input()`, que administra
 *   la totalidad de la lógica.
 * - Se deben inicializar las variables necesarias para el estado
 *   del juego: `rand1` para śostener el número aleatorio del
 *   equipo A, lo mismo para `rand2` con el equipo B. Dos contadores
 *   son necesarios para la cantidad de veces que el usuario ha
 *   ganado o perdido.
 * - Se inicia un bucle, con el propósito de repetir el juego
 *   infinitamente hasta cierta condición:
 *   - Se reinician las variables a su estado neutro.
 *   - Solicitar al usuario *dentro de un bucle* el equipo por el
 *     que apostará: A o B.
 *     - Si el usuario ha usado minúsculas, cambiarlas a mayúscula
 *     - Si el usuario entregó A o B, finalizar el búcle.
 *   - Generar dos números aleatorios para A y B, hasta que ambos
 *     sean diferentes.
 *   - Informar al usuario el número obtenido por A y por B.
 *   - Si es que la selección del usuario fue el equipo A:
 *     - Si el número de A es mayor al de B,
 *       - Informar al usuario que ha ganado
 *       - Añadir `+ 1` al contador de victorias.
 *     - Si no,
 *       - Informar al usuario que perdió
 *       - Añadir `+ 1` al contador de derrotas.
 *   - Si no, si la selección del usuario fue el equipo B:
 *     - Si el número de A es mayor al de B,
 *       - Informar al usuario que ha ganado
 *       - Añadir `+ 1` al contador de victorias.
 *     - Si no,
 *       - Informar al usuario que perdió
 *       - Añadir `+ 1` al contador de derrotas.
 *   - Informarle al usuario de la cantidad de veces que ha ganado
 *     o perdido, con el uso de los dos contadores.
 *   - Preguntar si es que se desea iniciar la apuesta otra vez,
 *     - Si la respuesta no es 's', romper el búcle.
 *   - Finalizar.
 *       
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define false 0
#define true 1

int aleatorio(int min, int max) {
	return (rand() % (max - min)) + min;
}

void leer_input() {
	int rand1,
		rand2,
		wins = 0,
		loses = 0;

	char selection;

	while (true) {
		selection = ' ';
		rand1 = 0;
		rand2 = 0;

		while (true) {
			printf("\n\nApuestas por A o por B?: ");
			scanf(" %c", &selection);
			
			if (selection == 'a')
				selection = 'A';

			if (selection == 'b')
				selection = 'B';

			if (selection == 'A' || selection == 'B')
				break;
		}

		while (rand1 == rand2) {
			rand1 = aleatorio(0, 10);
			rand2 = aleatorio(0, 10);
		}

		printf("A saca un %d y B saca un %d\n", rand1, rand2);

		if (selection == 'A') {
			if (rand1 > rand2) {
				printf("HAS GANADO!!!\n");
				wins++;
			} else {
				printf("HAS PERDIDO!!!\n");
				loses++;
			}
		} else if (selection == 'B') {
			if (rand1 < rand2) {
				printf("HAS GANADO!!!\n");
				wins++;
			} else {
				printf("HAS PERDIDO!!!\n");
				loses++;
			}
		}

		printf("Llevas %d partidas ganadas y %d partidas perdidas\n", wins, loses);

		printf("Quieres apostar otra vez? (S/N): ");
		scanf(" %c", &selection);

		if (selection == 's')
			selection = 'S';

		if (selection != 'S')
			break;
	}

	printf("Adios! :D\n");

	return;
}

int main() {
	srand(time(0));
	leer_input();

	return 0;
}
