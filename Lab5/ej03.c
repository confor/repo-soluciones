/* Apuestas
 * ========
 *
 * Notas:
 * - `int power(int n, int e)` devuelve el número `n` elevado
 *   a `e`, ya que no se puede usar `math.h`.
 * - Se ha implementado bubblesort usando los conocimientos
 *   externos a la clase, particularmente arrays.
 * 
 * Algoritmo narrado:
 * - Solicitar un número positivo al usuario.
 * - Con este número, contar la cantidad de digitos de este.
 * - Crear un array de tamaño (cantidad de digitos + 1)
 * - Se transforma el entero dado por el usuario a un array,
 *   donde cada uno de los elementos es uno de los digitos del
 *   número dado.
 *   - El límite superior será 10 elevado a la cantidad de digitos,
 *     el límite inferior será cero, y por cada ciclo del búcle
 *     se debe dividir el límite superior por 10.
 *   - Separar cada dígito del número dado y almacenarlo en
 *     el array previamente creado.
 * - Duplicar dos veces el array con los digitos del número en
 *   `menor` y `mayor`, para poder desordenarlas y transformarlas
 *   en un entero sin conflicto. Es posible que esto no sea necesario.
 * - Imprimir el número ingresado
 * - Llamar a `ínt numero_menor(int *menor, int size)` con el primer
 *   argumento siendo el array `menor` y el segundo argumento la cantidad
 *   de digitos que contiene el número dado.
 *   - Inicializar una variable para sostener el número menor.
 *   - Usar bubblesort para ordenar el array de menor a mayor.
 *   - Por cada miembro del array ordenado, sumarlo al número
 *     final multiplicado por diez elevado al índice actual.
 *     Esto devolverá un entero a partir del array ordenado.
 *   - Retornar el entero menor.
 * - Imprimir el menor número posible.
 * - Llamar a `ínt numero_mayor(int *mayor, int size)` con el primer
 *   argumento siendo el array `mayor` y el segundo argumento la cantidad
 *   de digitos que contiene el número dado.
 *   - Inicializar una variable para sostener el número mayor.
 *   - Usar bubblesort para ordenar el array de mayor a menor.
 *   - Por cada miembro del array ordenado, sumarlo al número
 *     final multiplicado por diez elevado al índice actual.
 *     Esto devolverá un entero a partir del array ordenado.
 *   - Retornar el entero mayor.
 * - Imprimir el mayor número posible.
 * - Finalizar.
 *
 */

#include <stdio.h>

int power(int n, int e) {
	int i = 0,
		resultado = 1;

	for(; i < e; i++)
		resultado *= n;

	return resultado;
}


int numero_menor(int *menor, int size) {
	int a,
		b,
		swap,
		nMenor = 0;

	for (a = 0; a < (size - 1); a++) {
		for(b = 0; b < size - a - 1; b++) {
			if (menor[b] < menor[b + 1]) {
				swap = menor[b];
				menor[b] = menor[b + 1];
				menor[b + 1] = swap;
			}
		}
	}

	for (int m = 0; m < size; m++)
		nMenor += menor[m] * power(10, m);

	return nMenor;
}

int numero_mayor(int *mayor, int size) {
	int a,
		b,
		swap,
		nMayor = 0;

	for (a = 0; a < (size - 1); a++) {
		for(b = 0; b < size - a - 1; b++) {
			if (mayor[b] > mayor[b + 1]) {
				swap = mayor[b];
				mayor[b] = mayor[b + 1];
				mayor[b + 1] = swap;
			}
		}
	}

	for (int m = 0; m < size; m++)
		nMayor += mayor[m] * power(10, m);

	return nMayor;
}

int main() {
	int n = 0;

	while(n <= 0) {
		printf("Ingrese un numero: ");
		scanf("%d", &n);
	}

	int size = 1;

	// se puede resumir con size = (int) floor(log base 10 (n))
	if (n > 10000000)
		size = 7;
	else if (n > 1000000)
		size = 6;
	else if (n > 100000)
		size = 5;
	else if (n > 10000)
		size = 4;
	else if (n > 1000)
		size = 3;
	else if (n > 100)
		size = 2;
	else if (n > 10)
		size = 1;

	int array[size + 1];

	int i = power(10, size),
		j = 0,
		x;

	for (; i > 0; ++j, i /= 10) {
		x = (n/i) % 10;
		array[j] = x;
	}

	// duplicar array, no se como se hace en c
	// :P
	int menor[size + 1],
		mayor[size + 1];

	for (int k = 0; k < j; k++) {
		menor[k] = array[k];
		mayor[k] = array[k];
	}

	printf("Numero ingresado: %d\n", n);
	printf("Numero menor que se puede formar: %d\n", numero_menor(menor, j));
	printf("Numero mayor que se puede formar: %d\n", numero_mayor(mayor, j));

	return 0;
}
