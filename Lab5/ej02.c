/*
 * Calcular raíz digital
 * =====================
 *
 * Algoritmo narrado:
 * - Se solicita un número al usuario.
 * - Con este número, se entrega a la función `int digitalroot(int n)`
 *   - Si el número es menor a 10, retornarlo.
 *   - El número será igual al último digito más la raiz digital
 *     del resto de sus digitos.
 *   - Devolver la raíz digital del número. Esto se asegura de que
 *     si el valor devuelto fue mayor a 10, se vuelve a sacar la raiz
 *     digital hasta que sea sólo un digito.
 * - Imprimir raiz digital.
 * - Finalizar.
 *
 *
 */

#include <stdio.h>

int digitalroot(int n) {
	if (n < 10)
		return n;

	n = n % 10 + digitalroot(n / 10);

	return digitalroot(n);
}

int main() {
	int X;
	printf("Ingrese un numero: ");
	scanf("%d", &X);

	printf("La raiz digital de %d es %d\n", X, digitalroot(X));

	return 0;
}
