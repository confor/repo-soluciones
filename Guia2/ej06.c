#include <stdio.h>

int main() {
	int n, total, i;
	int bool = 1;

	printf("n: ");
	scanf("%d", &n);

	for (i = 0; i < n; i++) {
		if(bool)
			bool = !bool;

		int temp = 1, j;

		for (j = 0; j < i; j++)
			temp *= i;

		if(bool)
			temp *= -1;

		total += temp;
	}

	printf("total: %d", total);

	return 0;
}