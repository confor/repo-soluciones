#include <stdio.h>

int main() {
	int hora, extra;

	// sanitización: sólo horas dado que [0, 24)
	while (hora < 0 || hora >= 24) {
		printf("Que hora es? ");
		scanf("%d", &hora);
	}

	printf("Cuantas horas agregar? ");
	scanf("%d", &extra);

	hora += extra;

	// los relojes no van mas allá de 24 horas,
	// quitarle 24 hasta que sea menor a 24
	while (hora >= 24) {
		hora -= 24;
	}

	printf("En %2d horas, el reloj marcara las %02d\n", extra, hora);

	return 0;
}
