#include <stdio.h>

int main() {
	int w, h, i, j;

	printf("Altura: ");
	scanf("%d", &h);
	printf("Ancho: ");
	scanf("%d", &w);

	if (h > 120)
		h = 120;

	if (w > 80)
		w = 80;

	for (i = 0; i < h; i++) {
		for (j = 0; j < w; j++) {
			printf("*");
		}

		printf("\n");
	}

	return 0;
}