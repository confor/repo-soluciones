#include <stdio.h>

int main() {
	int year;

	printf("Ingrese un anio: ");
	scanf("%d", &year);

	if (year < 1582) {
		if (year % 4 == 0) {
			printf("%d es bisiesto, segun el calendario juliano", year);
		} else {
			printf("%d no es anio bisiesto, segun el calendario juliano", year);
		}
	} else {
		if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
			printf("%d es bisiesto, segun el calendario gregoriano", year);
		} else {
			printf("%d no es anio bisiesto, segun el calendario gregoriano", year);
		}
	}

	return 0;
}
